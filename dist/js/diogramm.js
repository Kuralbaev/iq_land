
// Diogramm

$(document).ready(function () {
    $('#dtBasicExample').DataTable({
        // paging: false,
        "language": {
            "paginate": {
                "previous": "Назад"
            }
        },
        searching: false,
        info: false
    });
    $('.dataTables_length').addClass('bs-select');
});

//exporte les données sélectionnées
var $table = $('#table');
$(function () {
    $('#toolbar').find('select').change(function () {
        $table.bootstrapTable('refreshOptions', {
            exportDataType: $(this).val()
        });
    });
})

var trBoldBlue = $("table");

$(trBoldBlue).on("click", "tr", function () {
    $(this).toggleClass("bold-blue");
});

//Diogram Blue

var bar_ctx = document.getElementById('chart').getContext('2d');

var purple_orange_gradient = bar_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(bar_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: '',
            data: [12, 19, 3, 8, 14, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});



var b_ctx = document.getElementById('chart_1').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 15, 80, 20, 50, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var b_ctx = document.getElementById('chart_2').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 55, 80, 65, 10, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});

var b_ctx = document.getElementById('chart_3').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [63, 15, 80, 55, 50, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var b_ctx = document.getElementById('chart_4').getContext('2d');

var purple_orange_gradient = b_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#00aeef');
purple_orange_gradient.addColorStop(1, '#fff');

var bar_chart = new Chart(b_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [6, 10, 50, 6, 60, 95],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});



// Diogram Yellow

var yellow_ctx = document.getElementById('chart_yellow').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 15, 10, 20, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_6').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [1, 44, 19, 40, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_10').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [10, 15, 90, 20, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_11').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [90, 15, 90, 20, 25, 5],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});
var yellow_ctx = document.getElementById('chart_12').getContext('2d');

var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
purple_orange_gradient.addColorStop(0, '#f5d100 ');
purple_orange_gradient.addColorStop(1, '#5ecd74');

var bar_chart = new Chart(yellow_ctx, {
    type: 'line',
    data: {
        labels: ["Апрель ", "Май ", "Июнь", "Июль"],
        datasets: [{
            label: {},
            data: [50, 15, 10, 20, 25, 59],
            backgroundColor: purple_orange_gradient,
            hoverBackgroundColor: purple_orange_gradient,
            hoverBorderWidth: 2,
            hoverBorderColor: 'purple'
        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false
                }
            }],
            yAxes: [{
                gridLines: {
                    display: false
                }
            }]
        }
    }
});

// var yellow_ctx = document.getElementById('chart_7').getContext('2d');
//
// var purple_orange_gradient = yellow_ctx.createLinearGradient(0, 0, 0, 600);
// purple_orange_gradient.addColorStop(0, '#f5d100 ');
// purple_orange_gradient.addColorStop(1, '#5ecd74');


// Diogram Cercle

$(function () {
    $('.min-chart#chart-sales').easyPieChart({
        barColor: "#2ad474",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});
