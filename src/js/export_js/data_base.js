 $('.datepicker').pickadate({
     // Strings and translations
     monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь	', 'Ноябрь', 'Декабрь'],
     monthsShort: ['Янв', 'Феб', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
     weekdaysFull: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
     weekdaysShort: ['Пон', 'Втр', 'Срд', 'Чет', 'Пят', 'Суб', 'Вос'],
     showMonthsShort: undefined,
     showWeekdaysFull: undefined,


     // Buttons
     today: 'Сегодня',
     clear: 'Очистить',
     close: 'Закрыть',

     // Accessibility labels
     labelMonthNext: 'Next month',
     labelMonthPrev: 'Previous month',
     labelMonthSelect: 'Select a month',
     labelYearSelect: 'Select a year',

     // Formats
     format: 'd mmmm, yyyy',
     formatSubmit: undefined,
     hiddenPrefix: undefined,
     hiddenSuffix: '_submit',
     hiddenName: undefined,

     // Editable input
     editable: undefined,

     // Dropdown selectors
     selectYears: undefined,
     selectMonths: undefined,

     // First day of the week
     firstDay: undefined,

     // Date limits
     min: undefined,
     max: undefined,

     // Disable dates
     disable: undefined,

     // Root picker container
     container: undefined,

     // Hidden input container
     containerHidden: undefined,

     // Close on a user action
     closeOnSelect: true,
     closeOnClear: true,

     // Events
     onStart: undefined,
     onRender: undefined,
     onOpen: undefined,
     onClose: undefined,
     onSet: undefined,
     onStop: undefined,

     // Classes
     klass: {

         // The element states
         input: 'picker__input',
         active: 'picker__input--active',

         // The root picker and states *
         picker: 'picker',
         opened: 'picker--opened',
         focused: 'picker--focused',

         // The picker holder
         holder: 'picker__holder',

         // The picker frame, wrapper, and box
         frame: 'picker__frame',
         wrap: 'picker__wrap',
         box: 'picker__box',

         // The picker header
         header: 'picker__header',

         // Month navigation
         navPrev: 'picker__nav--prev',
         navNext: 'picker__nav--next',
         navDisabled: 'picker__nav--disabled',

         // Month & year labels
         month: 'picker__month',
         year: 'picker__year',

         // Month & year dropdowns
         selectMonth: 'picker__select--month',
         selectYear: 'picker__select--year',

         // Table of dates
         table: 'picker__table',

         // Weekday labels
         weekdays: 'picker__weekday',

         // Day states
         day: 'picker__day',
         disabled: 'picker__day--disabled',
         selected: 'picker__day--selected',
         highlighted: 'picker__day--highlighted',
         now: 'picker__day--today',
         infocus: 'picker__day--infocus',
         outfocus: 'picker__day--outfocus',

         // The picker footer
         footer: 'picker__footer',

         // Today, clear, & close buttons
         buttonClear: 'picker__button--clear',
         buttonClose: 'picker__button--close',
         buttonToday: 'picker__button--today'
     }
 });

 $('#input_starttime').pickatime({
     // 12 or 24 hour
     twelvehour: false,
 });
 $('#input_starttime_1').pickatime({
     // 12 or 24 hour
     twelvehour: false,
 });


 jQuery(document).ready(function () {
     jQuery(".icon").click(function () {
         jQuery(this).toggleClass("open");
         jQuery("#sidebar").toggleClass("active");
         jQuery("body").toggleClass("hidden")
     })
 });

 $(function () {
     $('.min-chart#chart-sales').easyPieChart({
         barColor: "#2ad474",
         onStep: function (from, to, percent) {
             $(this.el).find('.percent').text(Math.round(percent));
         }
     });
 });

 $(function () {
     $('.min-chart#chart-sales_red').easyPieChart({
         barColor: "#fe5656",
         onStep: function (from, to, percent) {
             $(this.el).find('.percent').text(Math.round(percent));
         }
     });
 });

 $(function () {
     $('.min-chart#chart-sales_yellow').easyPieChart({
         barColor: "#fed656",
         onStep: function (from, to, percent) {
             $(this.el).find('.percent').text(Math.round(percent));
         }
     });
 });
