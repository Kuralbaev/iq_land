//scss
import './../scss/admin.scss';
import './../scss/children.scss';
import './../scss/global.scss';
import './../scss/style.scss';
import './../scss/teacher.scss';

// Script


$(document).ready(function () {
    $(".icon").click(function () {
        $(this).toggleClass("open");
        $("#sidebar").toggleClass("active");
        $("body").toggleClass("hidden")
    })
});

$(function () {
    $('.min-chart#chart-sales').easyPieChart({
        barColor: "#2ad474",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});

$(function () {
    $('.min-chart#chart-sales_red').easyPieChart({
        barColor: "#fe5656",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});

$(function () {
    $('.min-chart#chart-sales_yellow').easyPieChart({
        barColor: "#fed656",
        onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
        }
    });
});


$(document).ready(function () {
    $('.mdb-select').material_select();
});


// Mask

$(function () {
    $("#time_1").mask("99 : 99");
    $("#time_2").mask("99 : 99");
    $(".phone").mask("+7 (999) 999 9999");
});


$('.select_custom').click(function(){
    $(this).toggleClass('open');
});


jQuery(function ($) {    
    $(document).mouseup(function (e) { // событие клика по веб-документу
        var div = $(".select_custom"); // тут указываем ID элемента
        // если клик был не по нашему блоку и не по его дочерним элементам 
        if (!div.is(e.target) && div.has(e.target).length === 0) { 
            $('.select_custom').removeClass('open'); // удаляем класс
        }
    });
});

jQuery(function ($) {    
    $(document).mouseup(function (e) { // событие клика по веб-документу
        var div = $(".alert_c"); // тут указываем ID элемента
        // если клик был не по нашему блоку и не по его дочерним элементам 
        if (!div.is(e.target) && div.has(e.target).length === 0) { 
            $('.alert_c').css({'display': 'none'}); // удаляем класс
        }
    });
});
$('.open_modal').click(function(){
    $('.modal_form_block').addClass('open');
});
$('.modal_close').click(function(){
    $('.modal_form_block').removeClass('open');
});